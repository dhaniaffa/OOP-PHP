<?php
require('animal.php');
require('frog.php');
require('ape.php');

echo "<h1>RELEASE 0 </h1>";

$sheep = new Animal("shaun");
echo "Nama = ".$sheep->name."<br>";
echo "Jumlah Kaki = ".$sheep->legs."<br>";
echo "Cold Blooded = ".$sheep->cold_blooded."<br>";

echo "<h1>RELEASE 1 </h1>";
$sungokong = new Ape("Kera Sakti");
echo "Nama = ".$sungokong->name."<br>";
echo "Jumlah Kaki = ".$sungokong->legs."<br>";
echo "Yell = ".$sungokong->yell();
echo "<br><br>";
$katak = new Frog("buduk");
echo "Nama = ".$katak->name."<br>";
echo "Jumlah Kaki = ".$katak->legs."<br>";
echo "Loncat = ".$katak->jump()."<br>";
?>